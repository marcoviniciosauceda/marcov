/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marcov;

/**
 *
 * @author marco
 */
public class Marcov {
    public class Agencia {
    private int noBoleto;
    private String nomCliente;
    private int años;
    private String destino;
    private int tipoViaje;
    private float precio;
    
    
    public   Agencia(){
        this.noBoleto = 0;
        this.destino = "";
        this.nomCliente = "";
        this.precio = 0.0f;
        this.años = 0;
        this.tipoViaje = 0;
           
    }

    public Agencia(int noBoleto, String nomCliente, float precio, String destino, int tipoViaje, int años) {
        this.años = años;
        this.destino = destino;
        this.noBoleto = noBoleto;
        this.nomCliente = nomCliente;
        this.precio = precio;
        this.tipoViaje = tipoViaje;
    }
    public Agencia(Agencia otro) {
        this.años = this.años;
        this.destino = this.destino;
        this.noBoleto = this.noBoleto;
        this.nomCliente = this.nomCliente;
        this.precio = this.precio;
        this.tipoViaje = this.tipoViaje;
    }

        public int getNoBoleto() {
            return noBoleto;
        }

        public void setNoBoleto(int noBoleto) {
            this.noBoleto = noBoleto;
        }

        public String getNomCliente() {
            return nomCliente;
        }

        public void setNomCliente(String nomCliente) {
            this.nomCliente = nomCliente;
        }

        public int getAños() {
            return años;
        }

        public void setAños(int años) {
            this.años = años;
        }

        public String getDestino() {
            return destino;
        }

        public void setDestino(String destino) {
            this.destino = destino;
        }

        public int getTipoViaje() {
            return tipoViaje;
        }

        public void setTipoViaje(int tipoViaje) {
            this.tipoViaje = tipoViaje;
        }
    

      public float calcularPrecioBoleto(){
        float precio =0.0f;
        switch(this.tipoViaje){
            case 1:
                precio= this.precio;
                break;
            case 2:
                precio = this.precio*1.8f;
                break;
        }
        return precio;
    } 
      public float calcularImpuesto(){
        float impuesto =0.0f;
        impuesto= this.precio*.16f;
        return impuesto;
            
    } 
      public float calcularDescuento(){
        float descuento=0.0f;
        if(this.años>60){
            descuento = this.calcularPrecioBoleto() * 0.5f;        }
        return descuento;
    }
      public float calcularTotal(){
        float totalPagar=0.0f;
        totalPagar = this.calcularPrecioBoleto() + this.calcularImpuesto() - this.calcularDescuento();       
        return totalPagar;
    }
      
    
    
  
     
    }
    }

    
